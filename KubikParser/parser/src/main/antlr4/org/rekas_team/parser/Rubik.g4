grammar Rubik;

init : start value (',' value)* end ;
value: init | movement;
start : '(' ;
end : ')' ;

lefts : 'L'|'L2'|'L\''|'L2\'';
rights : 'R'|'R2'|'R\''|'R2\'';
ups : 'U'|'U2'|'U\''|'U2\'';
downs : 'D'|'D2'|'D\''|'D2\'';
fronts : 'F'|'F2'|'F\''|'F2\'';
backs : 'B'|'B2'|'B\''|'B2\'';
middles : 'M'|'M2'|'M\''|'M2\'';
edges : 'E'|'E2'|'E\''|'E2\'';
sides : 'S'|'S2'|'S\''|'S2\'';
xaxis : 'X'|'X2'|'X\''|'X2\'';
yaxis : 'Y'|'Y2'|'Y\''|'Y2\'';
zaxis : 'Z'|'Z2'|'Z\''|'Z2\'';
//((L|R|U|D|F|B|M|E|S|X|Y|Z)(2|')*)(\s*)

movement : (lefts | rights | ups |
downs | fronts | backs |
middles | edges | sides |
xaxis | yaxis | zaxis
)*;
WS  : [ \t\r\n]+ -> skip ;
