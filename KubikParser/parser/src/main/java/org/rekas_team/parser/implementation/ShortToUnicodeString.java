package org.rekas_team.parser.implementation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.TerminalNode;
//import org.rekas_team.parser.ArrayInitBaseListener;
//import org.rekas_team.parser.ArrayInitParser;

/**
 * KubikParser - ShortToUnicodeString.
 * Example usage. Not related to the project but helps to test ANTLR4.
 *
 * @author marcoavilac
 * @since 6/18/2014
 */
public class ShortToUnicodeString
// extends ArrayInitBaseListener
{
//    private static final Logger LOGGER = LoggerFactory.getLogger(ShortToUnicodeString.class);
//
//    public ShortToUnicodeString(){
//        LOGGER.debug("New object");
//    }
//
//    @Override
//    public void enterValue(@NotNull ArrayInitParser.ValueContext ctx)
//    {
//        final TerminalNode terminalNode = ctx.INT();
//        if(terminalNode!=null)
//        {
//            final String terminalNodeText = terminalNode.getText();
//            if(terminalNodeText!=null)
//            {
//                final int value = Integer.valueOf(terminalNodeText);
//                LOGGER.debug(String.format("\\u%04x", value));
//
//            }
//        }
//    }
//
//    @Override
//    public void enterInit(@NotNull ArrayInitParser.InitContext ctx)
//    {
//        LOGGER.debug("{");
//    }
//
//    public void exitInit(@NotNull ArrayInitParser.InitContext ctx)
//    {
//        LOGGER.debug("}");
//    }
}
