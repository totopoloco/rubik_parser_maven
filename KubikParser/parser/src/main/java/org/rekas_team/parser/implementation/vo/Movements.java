package org.rekas_team.parser.implementation.vo;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * KubikParser - Movements
 *
 * @author marcoavilac
 * @since 7/17/2014
 */
@XmlRootElement(name = "movements")
@XmlAccessorType(XmlAccessType.FIELD)
public class Movements
{
    private static final Logger LOGGER = LoggerFactory.getLogger(Movements.class);

    public Movements()
    {
        LOGGER.debug("New instance.");
    }

    @XmlElement(name = "movement")
    private List<Movement> movements = null;

    public List<Movement> getMovements()
    {
        return this.movements;
    }

    public void setMovements(final List<Movement> movements)
    {
        this.movements = movements;
    }

    @Override
    public boolean equals(final Object o)
    {
        return EqualsBuilder.reflectionEquals(o, this);
    }

    @Override
    public int hashCode()
    {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public String toString()
    {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
