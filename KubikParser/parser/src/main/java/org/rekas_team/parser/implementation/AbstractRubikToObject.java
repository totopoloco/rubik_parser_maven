package org.rekas_team.parser.implementation;

import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.rekas_team.parser.RubikBaseVisitor;
import org.rekas_team.parser.implementation.vo.Direction;
import org.rekas_team.parser.implementation.vo.Movement;

/**
 * KubikParser - RubikToObjectVisitor
 *
 * @author marcoavilac
 * @since 7/15/2014
 */
public abstract class AbstractRubikToObject extends RubikBaseVisitor<Void>
{
    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractRubikToObject.class);


    public AbstractRubikToObject()
    {
        LOGGER.debug("New instance created");
    }


    /**
     *
     * @param token
     * @param direction
     * @return
     *
     * @throws java.lang.NullPointerException if parameters are null.
     */
    protected Movement createMovement(final String token, final Direction direction)
    {
        Validate.notNull(token);
        Validate.notNull(direction);

        final Movement movement;
        if (token.length() == 1)
        {
            movement = new Movement(direction, false, 1);
        }
        else if (token.length() == 2)
        {
            final char c = token.charAt(1);
            if(c == '\'')
            {
                movement = new Movement(direction, true, 2);
            } else {
                movement = new Movement(direction, false, 2);
            }
        }
        else
        {
            movement = new Movement(direction, true, 2);
        }
        return movement;
    }
}
