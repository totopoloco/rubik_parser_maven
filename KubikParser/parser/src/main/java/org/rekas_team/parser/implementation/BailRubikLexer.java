package org.rekas_team.parser.implementation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.LexerNoViableAltException;
import org.rekas_team.parser.RubikLexer;

/**
 * KubikParser - BailRubikLexer
 *
 * @author marcoavilac
 * @since 7/15/2014
 */
public class BailRubikLexer extends RubikLexer
{
    private static final Logger LOGGER = LoggerFactory.getLogger(BailRubikLexer.class);

    public BailRubikLexer(final CharStream charStream)
    {
        super(charStream);
    }

    @Override
    public void recover(LexerNoViableAltException lexerNoViableAltException)
    {
        LOGGER.warn("Caught LexerNoViableAltException {}", lexerNoViableAltException.toString());
        throw new RuntimeException(lexerNoViableAltException);
    }
}
