package org.rekas_team.parser.implementation.vo;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * KubikParser - Movement
 *
 * @author marcoavilac
 * @since 7/15/2014
 */
@XmlRootElement(name = "movement")
@XmlAccessorType(XmlAccessType.FIELD)
public final class Movement implements Serializable
{
    private static final long serialVersionUID = 2712272144722935215L;
    private final Direction direction;
    private final boolean clockwise;
    private final int times;

    public Movement()
    {
        this.direction = null;
        this.clockwise = false;
        this.times = -1;
    }

    public Movement(final Direction direction, final boolean clockwise, final int times)
    {
        this.direction = direction;
        this.clockwise = clockwise;
        this.times = times;
    }

    public Direction getDirection()
    {
        return this.direction;
    }

    public boolean isClockwise()
    {
        return this.clockwise;
    }

    public int getTimes()
    {
        return this.times;
    }

    @Override
    public boolean equals(final Object o)
    {
        return EqualsBuilder.reflectionEquals(o, this);
    }

    @Override
    public int hashCode()
    {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public String toString()
    {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
