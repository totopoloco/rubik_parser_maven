package org.rekas_team.parser.implementation.vo;

/**
 * KubikParser - Direction
 *
 * @author marcoavilac
 * @since 7/15/2014
 */
public enum Direction
{
    LEFT, RIGHT, UP, DOWN, FRONT, BACK, MIDDLE, EDGE, SIDE, XAXIS, YAXIS, ZAXIS;
}
