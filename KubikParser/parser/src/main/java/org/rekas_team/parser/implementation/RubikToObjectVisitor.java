package org.rekas_team.parser.implementation;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.antlr.v4.runtime.misc.NotNull;
import org.rekas_team.parser.RubikParser;
import org.rekas_team.parser.implementation.vo.Direction;
import org.rekas_team.parser.implementation.vo.Movement;

/**
 * KubikParser - RubikToObjectVisitor
 *
 * @author marcoavilac
 * @since 7/15/2014
 */
public class RubikToObjectVisitor extends AbstractRubikToObject
{
    private static final Logger LOGGER = LoggerFactory.getLogger(RubikToObjectVisitor.class);
    private final List<Movement> movements = new ArrayList<>();

    @Override
    public Void visitLefts(@NotNull final RubikParser.LeftsContext ctx)
    {
        final String ctxText = ctx.getText();
        this.movements.add(createMovement(ctxText, Direction.LEFT));

        LOGGER.debug("Context text: {}", ctxText);
        return super.visitLefts(ctx);
    }

    @Override
    public Void visitRights(@NotNull final RubikParser.RightsContext ctx)
    {
        LOGGER.debug("Context text: {}", ctx.getText());
        this.movements.add(createMovement(ctx.getText(), Direction.RIGHT));
        return super.visitRights(ctx);
    }

    @Override
    public Void visitFronts(@NotNull RubikParser.FrontsContext ctx)
    {
        LOGGER.debug("Context text: {}", ctx.getText());
        this.movements.add(createMovement(ctx.getText(), Direction.FRONT));

        return super.visitFronts(ctx);
    }

    @Override
    public Void visitBacks(@NotNull RubikParser.BacksContext ctx)
    {
        LOGGER.debug("Context text: {}", ctx.getText());
        this.movements.add(createMovement(ctx.getText(), Direction.BACK));

        return super.visitBacks(ctx);
    }


    @Override
    public Void visitUps(@NotNull RubikParser.UpsContext ctx)
    {
        LOGGER.debug("Context text: {}", ctx.getText());
        this.movements.add(createMovement(ctx.getText(), Direction.UP));
        return super.visitUps(ctx);
    }

    @Override
    public Void visitDowns(@NotNull RubikParser.DownsContext ctx)
    {
        LOGGER.debug("Context text: {}", ctx.getText());
        this.movements.add(createMovement(ctx.getText(), Direction.DOWN));
        return super.visitDowns(ctx);
    }

    @Override
    public Void visitMiddles(@NotNull RubikParser.MiddlesContext ctx)
    {
        LOGGER.debug("Context text: {}", ctx.getText());
        this.movements.add(createMovement(ctx.getText(), Direction.MIDDLE));
        return super.visitMiddles(ctx);
    }

    @Override
    public Void visitEdges(@NotNull RubikParser.EdgesContext ctx)
    {
        LOGGER.debug("Context text: {}", ctx.getText());
        this.movements.add(createMovement(ctx.getText(), Direction.EDGE));
        return super.visitEdges(ctx);
    }

    @Override
    public Void visitSides(@NotNull RubikParser.SidesContext ctx)
    {
        LOGGER.debug("Context text: {}", ctx.getText());
        this.movements.add(createMovement(ctx.getText(), Direction.SIDE));
        return super.visitSides(ctx);
    }

    @Override
    public Void visitXaxis(@NotNull RubikParser.XaxisContext ctx)
    {
        LOGGER.debug("Context text: {}", ctx.getText());
        this.movements.add(createMovement(ctx.getText(), Direction.XAXIS));
        return super.visitXaxis(ctx);
    }

    @Override
    public Void visitYaxis(@NotNull RubikParser.YaxisContext ctx)
    {
        LOGGER.debug("Context text: {}", ctx.getText());
        this.movements.add(createMovement(ctx.getText(), Direction.YAXIS));
        return super.visitYaxis(ctx);
    }

    @Override
    public Void visitZaxis(@NotNull RubikParser.ZaxisContext ctx)
    {
        LOGGER.debug("Context text: {}", ctx.getText());
        this.movements.add(createMovement(ctx.getText(), Direction.ZAXIS));
        return super.visitZaxis(ctx);
    }

    @Override
    public boolean equals(final Object o)
    {
        return EqualsBuilder.reflectionEquals(o, this);
    }

    @Override
    public int hashCode()
    {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public String toString()
    {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

    public List<Movement> getMovements()
    {
        return this.movements;
    }
}
