package org.rekas_team.parser.implementation;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.antlr.v4.runtime.ANTLRErrorStrategy;
import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.misc.NotNull;

/**
 * KubikParser - RubikErrorStrategy
 *
 * @author marcoavilac
 * @since 7/16/2014
 */
public class RubikErrorStrategy implements ANTLRErrorStrategy
{
    private static final Logger LOGGER = LoggerFactory.getLogger(RubikErrorStrategy.class);

    public RubikErrorStrategy()
    {
        LOGGER.debug("New instance.");
    }


    public void reset(@NotNull Parser recognizer)
    {

    }

    public Token recoverInline(@NotNull Parser recognizer) throws RecognitionException
    {
        throw new RuntimeException(new org.antlr.v4.runtime.InputMismatchException(recognizer));
    }

    public void recover(@NotNull Parser recognizer, @NotNull RecognitionException e) throws RecognitionException
    {
        throw new RuntimeException(e);
    }

    public void sync(@NotNull Parser recognizer) throws RecognitionException
    {

    }

    public boolean inErrorRecoveryMode(@NotNull Parser recognizer)
    {
        return false;
    }

    public void reportMatch(@NotNull Parser recognizer)
    {

    }

    public void reportError(@NotNull Parser recognizer, @NotNull RecognitionException e)
    {
        LOGGER.warn("Error caught {}", e.getMessage());
    }

    @Override
    public boolean equals(final Object o)
    {
        return EqualsBuilder.reflectionEquals(o, this);
    }

    @Override
    public int hashCode()
    {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public String toString()
    {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
