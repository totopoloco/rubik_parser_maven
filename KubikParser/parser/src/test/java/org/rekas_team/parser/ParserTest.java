package org.rekas_team.parser;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.rekas_team.parser.implementation.BailRubikLexer;
import org.rekas_team.parser.implementation.RubikErrorStrategy;
import org.rekas_team.parser.implementation.RubikToObjectVisitor;
import org.rekas_team.parser.implementation.vo.Direction;
import org.rekas_team.parser.implementation.vo.Movement;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * KubikParser - ParserTest
 *
 * @author marcoavilac
 * @since 6/18/2014
 */
public class ParserTest
{
    private static final Logger LOGGER = LoggerFactory.getLogger(ParserTest.class);

    @Test
    public void testRubikVisitor()
    {
        String input = "(L2,R,U2',(L,D,X))";
        LOGGER.debug(input);
        final ANTLRInputStream inputStream = new ANTLRInputStream(input);
        final BailRubikLexer rubikLexer = new BailRubikLexer(inputStream);

        final CommonTokenStream tokenStream = new CommonTokenStream(rubikLexer);
        RubikParser rubikParser = new RubikParser(tokenStream);
        rubikParser.setErrorHandler(new RubikErrorStrategy());
        ParseTree tree = rubikParser.init();

        RubikToObjectVisitor rubikToObjectVisitor = new RubikToObjectVisitor();
        LOGGER.debug("tree: {}", tree.toStringTree(rubikParser));
        rubikToObjectVisitor.visit(tree);

        List<Movement> movements = rubikToObjectVisitor.getMovements();
        List<Movement> expected = new ArrayList<Movement>();
        Movement movement0 = new Movement(Direction.LEFT, false, 2);
        Movement movement1 = new Movement(Direction.RIGHT, false, 1);
        Movement movement2 = new Movement(Direction.UP, true, 2);

        Movement movement3 = new Movement(Direction.LEFT, false, 1);
        Movement movement4 = new Movement(Direction.DOWN, false, 1);
        Movement movement5 = new Movement(Direction.XAXIS, false, 1);

        expected.add(movement0);
        expected.add(movement1);
        expected.add(movement2);
        expected.add(movement3);
        expected.add(movement4);
        expected.add(movement5);

        LOGGER.debug("Movements {}", movements);
        assertThat("list is expected with the correct parsed movements", movements, is(expected));
    }
}
