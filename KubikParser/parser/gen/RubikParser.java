// Generated from D:/shared/work_independent/rubik_parser/KubikParser/parser/src/main/antlr4/org/rekas_team/parser\Rubik.g4 by ANTLR 4.7
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class RubikParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.7", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		T__9=10, T__10=11, T__11=12, T__12=13, T__13=14, T__14=15, T__15=16, T__16=17, 
		T__17=18, T__18=19, T__19=20, T__20=21, T__21=22, T__22=23, T__23=24, 
		T__24=25, T__25=26, T__26=27, T__27=28, T__28=29, T__29=30, T__30=31, 
		T__31=32, T__32=33, T__33=34, T__34=35, T__35=36, T__36=37, T__37=38, 
		T__38=39, T__39=40, T__40=41, T__41=42, T__42=43, T__43=44, T__44=45, 
		T__45=46, T__46=47, T__47=48, T__48=49, T__49=50, T__50=51, WS=52;
	public static final int
		RULE_init = 0, RULE_value = 1, RULE_start = 2, RULE_end = 3, RULE_lefts = 4, 
		RULE_rights = 5, RULE_ups = 6, RULE_downs = 7, RULE_fronts = 8, RULE_backs = 9, 
		RULE_middles = 10, RULE_edges = 11, RULE_sides = 12, RULE_xaxis = 13, 
		RULE_yaxis = 14, RULE_zaxis = 15, RULE_movement = 16;
	public static final String[] ruleNames = {
		"init", "value", "start", "end", "lefts", "rights", "ups", "downs", "fronts", 
		"backs", "middles", "edges", "sides", "xaxis", "yaxis", "zaxis", "movement"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "','", "'('", "')'", "'L'", "'L2'", "'L''", "'L2''", "'R'", "'R2'", 
		"'R''", "'R2''", "'U'", "'U2'", "'U''", "'U2''", "'D'", "'D2'", "'D''", 
		"'D2''", "'F'", "'F2'", "'F''", "'F2''", "'B'", "'B2'", "'B''", "'B2''", 
		"'M'", "'M2'", "'M''", "'M2''", "'E'", "'E2'", "'E''", "'E2''", "'S'", 
		"'S2'", "'S''", "'S2''", "'X'", "'X2'", "'X''", "'X2''", "'Y'", "'Y2'", 
		"'Y''", "'Y2''", "'Z'", "'Z2'", "'Z''", "'Z2''"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, "WS"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "Rubik.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public RubikParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class InitContext extends ParserRuleContext {
		public StartContext start() {
			return getRuleContext(StartContext.class,0);
		}
		public List<ValueContext> value() {
			return getRuleContexts(ValueContext.class);
		}
		public ValueContext value(int i) {
			return getRuleContext(ValueContext.class,i);
		}
		public EndContext end() {
			return getRuleContext(EndContext.class,0);
		}
		public InitContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_init; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RubikListener ) ((RubikListener)listener).enterInit(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RubikListener ) ((RubikListener)listener).exitInit(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RubikVisitor ) return ((RubikVisitor<? extends T>)visitor).visitInit(this);
			else return visitor.visitChildren(this);
		}
	}

	public final InitContext init() throws RecognitionException {
		InitContext _localctx = new InitContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_init);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(34);
			start();
			setState(35);
			value();
			setState(40);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__0) {
				{
				{
				setState(36);
				match(T__0);
				setState(37);
				value();
				}
				}
				setState(42);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(43);
			end();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ValueContext extends ParserRuleContext {
		public InitContext init() {
			return getRuleContext(InitContext.class,0);
		}
		public MovementContext movement() {
			return getRuleContext(MovementContext.class,0);
		}
		public ValueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_value; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RubikListener ) ((RubikListener)listener).enterValue(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RubikListener ) ((RubikListener)listener).exitValue(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RubikVisitor ) return ((RubikVisitor<? extends T>)visitor).visitValue(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ValueContext value() throws RecognitionException {
		ValueContext _localctx = new ValueContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_value);
		try {
			setState(47);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__1:
				enterOuterAlt(_localctx, 1);
				{
				setState(45);
				init();
				}
				break;
			case T__0:
			case T__2:
			case T__3:
			case T__4:
			case T__5:
			case T__6:
			case T__7:
			case T__8:
			case T__9:
			case T__10:
			case T__11:
			case T__12:
			case T__13:
			case T__14:
			case T__15:
			case T__16:
			case T__17:
			case T__18:
			case T__19:
			case T__20:
			case T__21:
			case T__22:
			case T__23:
			case T__24:
			case T__25:
			case T__26:
			case T__27:
			case T__28:
			case T__29:
			case T__30:
			case T__31:
			case T__32:
			case T__33:
			case T__34:
			case T__35:
			case T__36:
			case T__37:
			case T__38:
			case T__39:
			case T__40:
			case T__41:
			case T__42:
			case T__43:
			case T__44:
			case T__45:
			case T__46:
			case T__47:
			case T__48:
			case T__49:
			case T__50:
				enterOuterAlt(_localctx, 2);
				{
				setState(46);
				movement();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StartContext extends ParserRuleContext {
		public StartContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_start; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RubikListener ) ((RubikListener)listener).enterStart(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RubikListener ) ((RubikListener)listener).exitStart(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RubikVisitor ) return ((RubikVisitor<? extends T>)visitor).visitStart(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StartContext start() throws RecognitionException {
		StartContext _localctx = new StartContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_start);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(49);
			match(T__1);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class EndContext extends ParserRuleContext {
		public EndContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_end; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RubikListener ) ((RubikListener)listener).enterEnd(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RubikListener ) ((RubikListener)listener).exitEnd(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RubikVisitor ) return ((RubikVisitor<? extends T>)visitor).visitEnd(this);
			else return visitor.visitChildren(this);
		}
	}

	public final EndContext end() throws RecognitionException {
		EndContext _localctx = new EndContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_end);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(51);
			match(T__2);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LeftsContext extends ParserRuleContext {
		public LeftsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_lefts; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RubikListener ) ((RubikListener)listener).enterLefts(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RubikListener ) ((RubikListener)listener).exitLefts(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RubikVisitor ) return ((RubikVisitor<? extends T>)visitor).visitLefts(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LeftsContext lefts() throws RecognitionException {
		LeftsContext _localctx = new LeftsContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_lefts);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(53);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__3) | (1L << T__4) | (1L << T__5) | (1L << T__6))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RightsContext extends ParserRuleContext {
		public RightsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_rights; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RubikListener ) ((RubikListener)listener).enterRights(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RubikListener ) ((RubikListener)listener).exitRights(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RubikVisitor ) return ((RubikVisitor<? extends T>)visitor).visitRights(this);
			else return visitor.visitChildren(this);
		}
	}

	public final RightsContext rights() throws RecognitionException {
		RightsContext _localctx = new RightsContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_rights);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(55);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__7) | (1L << T__8) | (1L << T__9) | (1L << T__10))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class UpsContext extends ParserRuleContext {
		public UpsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ups; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RubikListener ) ((RubikListener)listener).enterUps(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RubikListener ) ((RubikListener)listener).exitUps(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RubikVisitor ) return ((RubikVisitor<? extends T>)visitor).visitUps(this);
			else return visitor.visitChildren(this);
		}
	}

	public final UpsContext ups() throws RecognitionException {
		UpsContext _localctx = new UpsContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_ups);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(57);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__11) | (1L << T__12) | (1L << T__13) | (1L << T__14))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DownsContext extends ParserRuleContext {
		public DownsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_downs; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RubikListener ) ((RubikListener)listener).enterDowns(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RubikListener ) ((RubikListener)listener).exitDowns(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RubikVisitor ) return ((RubikVisitor<? extends T>)visitor).visitDowns(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DownsContext downs() throws RecognitionException {
		DownsContext _localctx = new DownsContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_downs);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(59);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__15) | (1L << T__16) | (1L << T__17) | (1L << T__18))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FrontsContext extends ParserRuleContext {
		public FrontsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_fronts; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RubikListener ) ((RubikListener)listener).enterFronts(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RubikListener ) ((RubikListener)listener).exitFronts(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RubikVisitor ) return ((RubikVisitor<? extends T>)visitor).visitFronts(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FrontsContext fronts() throws RecognitionException {
		FrontsContext _localctx = new FrontsContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_fronts);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(61);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__19) | (1L << T__20) | (1L << T__21) | (1L << T__22))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BacksContext extends ParserRuleContext {
		public BacksContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_backs; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RubikListener ) ((RubikListener)listener).enterBacks(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RubikListener ) ((RubikListener)listener).exitBacks(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RubikVisitor ) return ((RubikVisitor<? extends T>)visitor).visitBacks(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BacksContext backs() throws RecognitionException {
		BacksContext _localctx = new BacksContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_backs);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(63);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__23) | (1L << T__24) | (1L << T__25) | (1L << T__26))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MiddlesContext extends ParserRuleContext {
		public MiddlesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_middles; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RubikListener ) ((RubikListener)listener).enterMiddles(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RubikListener ) ((RubikListener)listener).exitMiddles(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RubikVisitor ) return ((RubikVisitor<? extends T>)visitor).visitMiddles(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MiddlesContext middles() throws RecognitionException {
		MiddlesContext _localctx = new MiddlesContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_middles);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(65);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__27) | (1L << T__28) | (1L << T__29) | (1L << T__30))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class EdgesContext extends ParserRuleContext {
		public EdgesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_edges; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RubikListener ) ((RubikListener)listener).enterEdges(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RubikListener ) ((RubikListener)listener).exitEdges(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RubikVisitor ) return ((RubikVisitor<? extends T>)visitor).visitEdges(this);
			else return visitor.visitChildren(this);
		}
	}

	public final EdgesContext edges() throws RecognitionException {
		EdgesContext _localctx = new EdgesContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_edges);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(67);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__31) | (1L << T__32) | (1L << T__33) | (1L << T__34))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SidesContext extends ParserRuleContext {
		public SidesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_sides; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RubikListener ) ((RubikListener)listener).enterSides(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RubikListener ) ((RubikListener)listener).exitSides(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RubikVisitor ) return ((RubikVisitor<? extends T>)visitor).visitSides(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SidesContext sides() throws RecognitionException {
		SidesContext _localctx = new SidesContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_sides);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(69);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__35) | (1L << T__36) | (1L << T__37) | (1L << T__38))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class XaxisContext extends ParserRuleContext {
		public XaxisContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_xaxis; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RubikListener ) ((RubikListener)listener).enterXaxis(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RubikListener ) ((RubikListener)listener).exitXaxis(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RubikVisitor ) return ((RubikVisitor<? extends T>)visitor).visitXaxis(this);
			else return visitor.visitChildren(this);
		}
	}

	public final XaxisContext xaxis() throws RecognitionException {
		XaxisContext _localctx = new XaxisContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_xaxis);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(71);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__39) | (1L << T__40) | (1L << T__41) | (1L << T__42))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class YaxisContext extends ParserRuleContext {
		public YaxisContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_yaxis; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RubikListener ) ((RubikListener)listener).enterYaxis(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RubikListener ) ((RubikListener)listener).exitYaxis(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RubikVisitor ) return ((RubikVisitor<? extends T>)visitor).visitYaxis(this);
			else return visitor.visitChildren(this);
		}
	}

	public final YaxisContext yaxis() throws RecognitionException {
		YaxisContext _localctx = new YaxisContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_yaxis);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(73);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__43) | (1L << T__44) | (1L << T__45) | (1L << T__46))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ZaxisContext extends ParserRuleContext {
		public ZaxisContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_zaxis; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RubikListener ) ((RubikListener)listener).enterZaxis(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RubikListener ) ((RubikListener)listener).exitZaxis(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RubikVisitor ) return ((RubikVisitor<? extends T>)visitor).visitZaxis(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ZaxisContext zaxis() throws RecognitionException {
		ZaxisContext _localctx = new ZaxisContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_zaxis);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(75);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__47) | (1L << T__48) | (1L << T__49) | (1L << T__50))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MovementContext extends ParserRuleContext {
		public List<LeftsContext> lefts() {
			return getRuleContexts(LeftsContext.class);
		}
		public LeftsContext lefts(int i) {
			return getRuleContext(LeftsContext.class,i);
		}
		public List<RightsContext> rights() {
			return getRuleContexts(RightsContext.class);
		}
		public RightsContext rights(int i) {
			return getRuleContext(RightsContext.class,i);
		}
		public List<UpsContext> ups() {
			return getRuleContexts(UpsContext.class);
		}
		public UpsContext ups(int i) {
			return getRuleContext(UpsContext.class,i);
		}
		public List<DownsContext> downs() {
			return getRuleContexts(DownsContext.class);
		}
		public DownsContext downs(int i) {
			return getRuleContext(DownsContext.class,i);
		}
		public List<FrontsContext> fronts() {
			return getRuleContexts(FrontsContext.class);
		}
		public FrontsContext fronts(int i) {
			return getRuleContext(FrontsContext.class,i);
		}
		public List<BacksContext> backs() {
			return getRuleContexts(BacksContext.class);
		}
		public BacksContext backs(int i) {
			return getRuleContext(BacksContext.class,i);
		}
		public List<MiddlesContext> middles() {
			return getRuleContexts(MiddlesContext.class);
		}
		public MiddlesContext middles(int i) {
			return getRuleContext(MiddlesContext.class,i);
		}
		public List<EdgesContext> edges() {
			return getRuleContexts(EdgesContext.class);
		}
		public EdgesContext edges(int i) {
			return getRuleContext(EdgesContext.class,i);
		}
		public List<SidesContext> sides() {
			return getRuleContexts(SidesContext.class);
		}
		public SidesContext sides(int i) {
			return getRuleContext(SidesContext.class,i);
		}
		public List<XaxisContext> xaxis() {
			return getRuleContexts(XaxisContext.class);
		}
		public XaxisContext xaxis(int i) {
			return getRuleContext(XaxisContext.class,i);
		}
		public List<YaxisContext> yaxis() {
			return getRuleContexts(YaxisContext.class);
		}
		public YaxisContext yaxis(int i) {
			return getRuleContext(YaxisContext.class,i);
		}
		public List<ZaxisContext> zaxis() {
			return getRuleContexts(ZaxisContext.class);
		}
		public ZaxisContext zaxis(int i) {
			return getRuleContext(ZaxisContext.class,i);
		}
		public MovementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_movement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RubikListener ) ((RubikListener)listener).enterMovement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RubikListener ) ((RubikListener)listener).exitMovement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RubikVisitor ) return ((RubikVisitor<? extends T>)visitor).visitMovement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MovementContext movement() throws RecognitionException {
		MovementContext _localctx = new MovementContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_movement);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(91);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__3) | (1L << T__4) | (1L << T__5) | (1L << T__6) | (1L << T__7) | (1L << T__8) | (1L << T__9) | (1L << T__10) | (1L << T__11) | (1L << T__12) | (1L << T__13) | (1L << T__14) | (1L << T__15) | (1L << T__16) | (1L << T__17) | (1L << T__18) | (1L << T__19) | (1L << T__20) | (1L << T__21) | (1L << T__22) | (1L << T__23) | (1L << T__24) | (1L << T__25) | (1L << T__26) | (1L << T__27) | (1L << T__28) | (1L << T__29) | (1L << T__30) | (1L << T__31) | (1L << T__32) | (1L << T__33) | (1L << T__34) | (1L << T__35) | (1L << T__36) | (1L << T__37) | (1L << T__38) | (1L << T__39) | (1L << T__40) | (1L << T__41) | (1L << T__42) | (1L << T__43) | (1L << T__44) | (1L << T__45) | (1L << T__46) | (1L << T__47) | (1L << T__48) | (1L << T__49) | (1L << T__50))) != 0)) {
				{
				setState(89);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case T__3:
				case T__4:
				case T__5:
				case T__6:
					{
					setState(77);
					lefts();
					}
					break;
				case T__7:
				case T__8:
				case T__9:
				case T__10:
					{
					setState(78);
					rights();
					}
					break;
				case T__11:
				case T__12:
				case T__13:
				case T__14:
					{
					setState(79);
					ups();
					}
					break;
				case T__15:
				case T__16:
				case T__17:
				case T__18:
					{
					setState(80);
					downs();
					}
					break;
				case T__19:
				case T__20:
				case T__21:
				case T__22:
					{
					setState(81);
					fronts();
					}
					break;
				case T__23:
				case T__24:
				case T__25:
				case T__26:
					{
					setState(82);
					backs();
					}
					break;
				case T__27:
				case T__28:
				case T__29:
				case T__30:
					{
					setState(83);
					middles();
					}
					break;
				case T__31:
				case T__32:
				case T__33:
				case T__34:
					{
					setState(84);
					edges();
					}
					break;
				case T__35:
				case T__36:
				case T__37:
				case T__38:
					{
					setState(85);
					sides();
					}
					break;
				case T__39:
				case T__40:
				case T__41:
				case T__42:
					{
					setState(86);
					xaxis();
					}
					break;
				case T__43:
				case T__44:
				case T__45:
				case T__46:
					{
					setState(87);
					yaxis();
					}
					break;
				case T__47:
				case T__48:
				case T__49:
				case T__50:
					{
					setState(88);
					zaxis();
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				setState(93);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\66a\4\2\t\2\4\3\t"+
		"\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4"+
		"\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22\3\2\3"+
		"\2\3\2\3\2\7\2)\n\2\f\2\16\2,\13\2\3\2\3\2\3\3\3\3\5\3\62\n\3\3\4\3\4"+
		"\3\5\3\5\3\6\3\6\3\7\3\7\3\b\3\b\3\t\3\t\3\n\3\n\3\13\3\13\3\f\3\f\3\r"+
		"\3\r\3\16\3\16\3\17\3\17\3\20\3\20\3\21\3\21\3\22\3\22\3\22\3\22\3\22"+
		"\3\22\3\22\3\22\3\22\3\22\3\22\3\22\7\22\\\n\22\f\22\16\22_\13\22\3\22"+
		"\2\2\23\2\4\6\b\n\f\16\20\22\24\26\30\32\34\36 \"\2\16\3\2\6\t\3\2\n\r"+
		"\3\2\16\21\3\2\22\25\3\2\26\31\3\2\32\35\3\2\36!\3\2\"%\3\2&)\3\2*-\3"+
		"\2.\61\3\2\62\65\2]\2$\3\2\2\2\4\61\3\2\2\2\6\63\3\2\2\2\b\65\3\2\2\2"+
		"\n\67\3\2\2\2\f9\3\2\2\2\16;\3\2\2\2\20=\3\2\2\2\22?\3\2\2\2\24A\3\2\2"+
		"\2\26C\3\2\2\2\30E\3\2\2\2\32G\3\2\2\2\34I\3\2\2\2\36K\3\2\2\2 M\3\2\2"+
		"\2\"]\3\2\2\2$%\5\6\4\2%*\5\4\3\2&\'\7\3\2\2\')\5\4\3\2(&\3\2\2\2),\3"+
		"\2\2\2*(\3\2\2\2*+\3\2\2\2+-\3\2\2\2,*\3\2\2\2-.\5\b\5\2.\3\3\2\2\2/\62"+
		"\5\2\2\2\60\62\5\"\22\2\61/\3\2\2\2\61\60\3\2\2\2\62\5\3\2\2\2\63\64\7"+
		"\4\2\2\64\7\3\2\2\2\65\66\7\5\2\2\66\t\3\2\2\2\678\t\2\2\28\13\3\2\2\2"+
		"9:\t\3\2\2:\r\3\2\2\2;<\t\4\2\2<\17\3\2\2\2=>\t\5\2\2>\21\3\2\2\2?@\t"+
		"\6\2\2@\23\3\2\2\2AB\t\7\2\2B\25\3\2\2\2CD\t\b\2\2D\27\3\2\2\2EF\t\t\2"+
		"\2F\31\3\2\2\2GH\t\n\2\2H\33\3\2\2\2IJ\t\13\2\2J\35\3\2\2\2KL\t\f\2\2"+
		"L\37\3\2\2\2MN\t\r\2\2N!\3\2\2\2O\\\5\n\6\2P\\\5\f\7\2Q\\\5\16\b\2R\\"+
		"\5\20\t\2S\\\5\22\n\2T\\\5\24\13\2U\\\5\26\f\2V\\\5\30\r\2W\\\5\32\16"+
		"\2X\\\5\34\17\2Y\\\5\36\20\2Z\\\5 \21\2[O\3\2\2\2[P\3\2\2\2[Q\3\2\2\2"+
		"[R\3\2\2\2[S\3\2\2\2[T\3\2\2\2[U\3\2\2\2[V\3\2\2\2[W\3\2\2\2[X\3\2\2\2"+
		"[Y\3\2\2\2[Z\3\2\2\2\\_\3\2\2\2][\3\2\2\2]^\3\2\2\2^#\3\2\2\2_]\3\2\2"+
		"\2\6*\61[]";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}