// Generated from D:/shared/work_independent/rubik_parser/KubikParser/parser/src/main/antlr4/org/rekas_team/parser\Rubik.g4 by ANTLR 4.7
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link RubikParser}.
 */
public interface RubikListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link RubikParser#init}.
	 * @param ctx the parse tree
	 */
	void enterInit(RubikParser.InitContext ctx);
	/**
	 * Exit a parse tree produced by {@link RubikParser#init}.
	 * @param ctx the parse tree
	 */
	void exitInit(RubikParser.InitContext ctx);
	/**
	 * Enter a parse tree produced by {@link RubikParser#value}.
	 * @param ctx the parse tree
	 */
	void enterValue(RubikParser.ValueContext ctx);
	/**
	 * Exit a parse tree produced by {@link RubikParser#value}.
	 * @param ctx the parse tree
	 */
	void exitValue(RubikParser.ValueContext ctx);
	/**
	 * Enter a parse tree produced by {@link RubikParser#start}.
	 * @param ctx the parse tree
	 */
	void enterStart(RubikParser.StartContext ctx);
	/**
	 * Exit a parse tree produced by {@link RubikParser#start}.
	 * @param ctx the parse tree
	 */
	void exitStart(RubikParser.StartContext ctx);
	/**
	 * Enter a parse tree produced by {@link RubikParser#end}.
	 * @param ctx the parse tree
	 */
	void enterEnd(RubikParser.EndContext ctx);
	/**
	 * Exit a parse tree produced by {@link RubikParser#end}.
	 * @param ctx the parse tree
	 */
	void exitEnd(RubikParser.EndContext ctx);
	/**
	 * Enter a parse tree produced by {@link RubikParser#lefts}.
	 * @param ctx the parse tree
	 */
	void enterLefts(RubikParser.LeftsContext ctx);
	/**
	 * Exit a parse tree produced by {@link RubikParser#lefts}.
	 * @param ctx the parse tree
	 */
	void exitLefts(RubikParser.LeftsContext ctx);
	/**
	 * Enter a parse tree produced by {@link RubikParser#rights}.
	 * @param ctx the parse tree
	 */
	void enterRights(RubikParser.RightsContext ctx);
	/**
	 * Exit a parse tree produced by {@link RubikParser#rights}.
	 * @param ctx the parse tree
	 */
	void exitRights(RubikParser.RightsContext ctx);
	/**
	 * Enter a parse tree produced by {@link RubikParser#ups}.
	 * @param ctx the parse tree
	 */
	void enterUps(RubikParser.UpsContext ctx);
	/**
	 * Exit a parse tree produced by {@link RubikParser#ups}.
	 * @param ctx the parse tree
	 */
	void exitUps(RubikParser.UpsContext ctx);
	/**
	 * Enter a parse tree produced by {@link RubikParser#downs}.
	 * @param ctx the parse tree
	 */
	void enterDowns(RubikParser.DownsContext ctx);
	/**
	 * Exit a parse tree produced by {@link RubikParser#downs}.
	 * @param ctx the parse tree
	 */
	void exitDowns(RubikParser.DownsContext ctx);
	/**
	 * Enter a parse tree produced by {@link RubikParser#fronts}.
	 * @param ctx the parse tree
	 */
	void enterFronts(RubikParser.FrontsContext ctx);
	/**
	 * Exit a parse tree produced by {@link RubikParser#fronts}.
	 * @param ctx the parse tree
	 */
	void exitFronts(RubikParser.FrontsContext ctx);
	/**
	 * Enter a parse tree produced by {@link RubikParser#backs}.
	 * @param ctx the parse tree
	 */
	void enterBacks(RubikParser.BacksContext ctx);
	/**
	 * Exit a parse tree produced by {@link RubikParser#backs}.
	 * @param ctx the parse tree
	 */
	void exitBacks(RubikParser.BacksContext ctx);
	/**
	 * Enter a parse tree produced by {@link RubikParser#middles}.
	 * @param ctx the parse tree
	 */
	void enterMiddles(RubikParser.MiddlesContext ctx);
	/**
	 * Exit a parse tree produced by {@link RubikParser#middles}.
	 * @param ctx the parse tree
	 */
	void exitMiddles(RubikParser.MiddlesContext ctx);
	/**
	 * Enter a parse tree produced by {@link RubikParser#edges}.
	 * @param ctx the parse tree
	 */
	void enterEdges(RubikParser.EdgesContext ctx);
	/**
	 * Exit a parse tree produced by {@link RubikParser#edges}.
	 * @param ctx the parse tree
	 */
	void exitEdges(RubikParser.EdgesContext ctx);
	/**
	 * Enter a parse tree produced by {@link RubikParser#sides}.
	 * @param ctx the parse tree
	 */
	void enterSides(RubikParser.SidesContext ctx);
	/**
	 * Exit a parse tree produced by {@link RubikParser#sides}.
	 * @param ctx the parse tree
	 */
	void exitSides(RubikParser.SidesContext ctx);
	/**
	 * Enter a parse tree produced by {@link RubikParser#xaxis}.
	 * @param ctx the parse tree
	 */
	void enterXaxis(RubikParser.XaxisContext ctx);
	/**
	 * Exit a parse tree produced by {@link RubikParser#xaxis}.
	 * @param ctx the parse tree
	 */
	void exitXaxis(RubikParser.XaxisContext ctx);
	/**
	 * Enter a parse tree produced by {@link RubikParser#yaxis}.
	 * @param ctx the parse tree
	 */
	void enterYaxis(RubikParser.YaxisContext ctx);
	/**
	 * Exit a parse tree produced by {@link RubikParser#yaxis}.
	 * @param ctx the parse tree
	 */
	void exitYaxis(RubikParser.YaxisContext ctx);
	/**
	 * Enter a parse tree produced by {@link RubikParser#zaxis}.
	 * @param ctx the parse tree
	 */
	void enterZaxis(RubikParser.ZaxisContext ctx);
	/**
	 * Exit a parse tree produced by {@link RubikParser#zaxis}.
	 * @param ctx the parse tree
	 */
	void exitZaxis(RubikParser.ZaxisContext ctx);
	/**
	 * Enter a parse tree produced by {@link RubikParser#movement}.
	 * @param ctx the parse tree
	 */
	void enterMovement(RubikParser.MovementContext ctx);
	/**
	 * Exit a parse tree produced by {@link RubikParser#movement}.
	 * @param ctx the parse tree
	 */
	void exitMovement(RubikParser.MovementContext ctx);
}