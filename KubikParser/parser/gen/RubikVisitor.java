// Generated from D:/shared/work_independent/rubik_parser/KubikParser/parser/src/main/antlr4/org/rekas_team/parser\Rubik.g4 by ANTLR 4.7
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link RubikParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface RubikVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link RubikParser#init}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInit(RubikParser.InitContext ctx);
	/**
	 * Visit a parse tree produced by {@link RubikParser#value}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitValue(RubikParser.ValueContext ctx);
	/**
	 * Visit a parse tree produced by {@link RubikParser#start}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStart(RubikParser.StartContext ctx);
	/**
	 * Visit a parse tree produced by {@link RubikParser#end}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEnd(RubikParser.EndContext ctx);
	/**
	 * Visit a parse tree produced by {@link RubikParser#lefts}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLefts(RubikParser.LeftsContext ctx);
	/**
	 * Visit a parse tree produced by {@link RubikParser#rights}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRights(RubikParser.RightsContext ctx);
	/**
	 * Visit a parse tree produced by {@link RubikParser#ups}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUps(RubikParser.UpsContext ctx);
	/**
	 * Visit a parse tree produced by {@link RubikParser#downs}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDowns(RubikParser.DownsContext ctx);
	/**
	 * Visit a parse tree produced by {@link RubikParser#fronts}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFronts(RubikParser.FrontsContext ctx);
	/**
	 * Visit a parse tree produced by {@link RubikParser#backs}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBacks(RubikParser.BacksContext ctx);
	/**
	 * Visit a parse tree produced by {@link RubikParser#middles}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMiddles(RubikParser.MiddlesContext ctx);
	/**
	 * Visit a parse tree produced by {@link RubikParser#edges}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEdges(RubikParser.EdgesContext ctx);
	/**
	 * Visit a parse tree produced by {@link RubikParser#sides}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSides(RubikParser.SidesContext ctx);
	/**
	 * Visit a parse tree produced by {@link RubikParser#xaxis}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitXaxis(RubikParser.XaxisContext ctx);
	/**
	 * Visit a parse tree produced by {@link RubikParser#yaxis}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitYaxis(RubikParser.YaxisContext ctx);
	/**
	 * Visit a parse tree produced by {@link RubikParser#zaxis}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitZaxis(RubikParser.ZaxisContext ctx);
	/**
	 * Visit a parse tree produced by {@link RubikParser#movement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMovement(RubikParser.MovementContext ctx);
}