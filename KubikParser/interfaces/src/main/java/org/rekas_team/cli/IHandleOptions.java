package org.rekas_team.cli;

import java.util.List;
import java.util.Map;

/**
 * KubikParser - IHandleOptions
 *
 * @author marcoavilac
 * @since 6/17/2014
 */
public interface IHandleOptions
{
    Map<String,List<String>> handle();
}
