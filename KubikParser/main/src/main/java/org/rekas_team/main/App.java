package org.rekas_team.main;

import java.io.File;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.util.List;
import java.util.Map;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.rekas.team.cli.HandleOptionsImpl;
import org.rekas_team.cli.IHandleOptions;
import org.rekas_team.parser.RubikParser;
import org.rekas_team.parser.implementation.BailRubikLexer;
import org.rekas_team.parser.implementation.RubikErrorStrategy;
import org.rekas_team.parser.implementation.RubikToObjectVisitor;
import org.rekas_team.parser.implementation.vo.Movement;
import org.rekas_team.parser.implementation.vo.Movements;

/**
 * Starting point of the application.
 */
public class App
{
    private static final Logger LOGGER = LoggerFactory.getLogger(App.class);


    public static void main(final String[] args) throws JAXBException
    {
        final IHandleOptions handleOptions = new HandleOptionsImpl(args);
        final Map<String, List<String>> options = handleOptions.handle();
        final List<String> patterns = options.get(HandleOptionsImpl.RUBIK_PATTERN);
        int n = 0;

        for (final String pattern : patterns)
        {

            final ANTLRInputStream inputStream = new ANTLRInputStream(pattern);
            final BailRubikLexer rubikLexer = new BailRubikLexer(inputStream);

            final CommonTokenStream tokenStream = new CommonTokenStream(rubikLexer);
            RubikParser rubikParser = new RubikParser(tokenStream);
            rubikParser.setErrorHandler(new RubikErrorStrategy());
            ParseTree tree = rubikParser.init();

            RubikToObjectVisitor rubikToObjectVisitor = new RubikToObjectVisitor();
            LOGGER.debug("tree: {}", tree.toStringTree(rubikParser));
            rubikToObjectVisitor.visit(tree);

            Movements movements = new Movements();
            List<Movement> movementsList = rubikToObjectVisitor.getMovements();

            LOGGER.debug("Movements {} " + movementsList);
            movements.setMovements(movementsList);
            marshalingMovements(n++, movements, options);
        }
    }


    private static void marshalingMovements(final int n, final Movements movements,
                                            final Map<String, List<String>> options) throws JAXBException
    {

        if (options.containsKey(HandleOptionsImpl.OUTPUT_DIRECTORY))
        {
            final List<String> strings = options.get(HandleOptionsImpl.OUTPUT_DIRECTORY);
            String outputDir = strings.get(0);


            JAXBContext jaxbContext = JAXBContext.newInstance(Movements.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            //Marshal the movement list in console
            //jaxbMarshaller.
            //jaxbMarshaller.marshal(movements, System.out);

            //Marshal the movement list in file
            FileSystem fileSystem = FileSystems.getDefault();
            final String pathname = outputDir + fileSystem.getSeparator() + "movements" + n + ".xml";
            jaxbMarshaller.marshal(movements, new File(pathname));
        }
    }
}
