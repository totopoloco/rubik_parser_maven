package org.rekas.team.cli;

import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.rekas_team.cli.IHandleOptions;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;

public class HandleOptionsImplTest
{
    private IHandleOptions iHandleOptions;
    private static final Logger LOGGER = LoggerFactory.getLogger(HandleOptionsImplTest.class);

    @Test
    public void testOptionsHelp() throws Exception
    {
        final String[] args = new String[]{ "-help" };
        iHandleOptions = new HandleOptionsImpl(args);
        final Map<String, List<String>> rubikMovements = iHandleOptions.handle();
        LOGGER.debug("Was handled {}", rubikMovements);
        assertThat("Was handled ok", rubikMovements.get(HandleOptionsImpl.RUBIK_PATTERN), hasSize(0));
    }

    @Test
    public void testOptionsMovements() throws Exception
    {
        final String[] args = new String[]{ "-rubik_pattern", "F L' U2 R' U'" };
        iHandleOptions = new HandleOptionsImpl(args);
        final  Map<String, List<String>> rubikMovements = iHandleOptions.handle();
        LOGGER.debug("Was handled {}", rubikMovements);
        assertThat("Was handled ok", rubikMovements.get(HandleOptionsImpl.RUBIK_PATTERN), hasSize(1));
    }
}