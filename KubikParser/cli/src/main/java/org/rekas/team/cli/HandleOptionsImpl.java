package org.rekas.team.cli;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.rekas_team.cli.IHandleOptions;

import static org.apache.commons.lang3.builder.ToStringStyle.SHORT_PREFIX_STYLE;


/**
 * KubikParser - HandleOptions
 *
 * @author marcoavilac
 * @since 6/17/2014
 */
public final class HandleOptionsImpl implements IHandleOptions, Serializable
{
    public static final String RUBIK_PATTERN = "rubik_pattern";
    public static final String FILE_PATERN = "file_pattern";
    public static final String OUTPUT_DIRECTORY = "output_dir";
    private static final long serialVersionUID = -846361266316270922L;
    private static final Logger LOGGER = LoggerFactory.getLogger(HandleOptionsImpl.class);
    private final String[] args;

    public HandleOptionsImpl(final String[] args)
    {
        this.args = args;
    }

    @Override
    public Map<String, List<String>> handle()
    {
        final Options options = getOptions();
        // boolean isCorrectlyHandled = false;
        Map<String, List<String>> rubikMovesToBeParser = Collections.EMPTY_MAP;
        try
        {
            rubikMovesToBeParser = parseOptions(options);
        }
        catch (ParseException e)
        {
            LOGGER.error("ParseException caught: {} ", e);
            showUsage(options);
        }
        return rubikMovesToBeParser;
    }

    private static Options getOptions()
    {
        final Option help = new Option("help", "print this message");
        final Option rubik_pattern = OptionBuilder.withArgName("pattern")
                                                  .hasArg()
                                                  .withDescription("Rubik pattern describing the movements in the cube")
                                                  .create(RUBIK_PATTERN);
        final Option file_pattern = OptionBuilder.withArgName("file")
                                                 .hasArg()
                                                 .withDescription("File pattern with patterns separated by LF/CR")
                                                 .create(FILE_PATERN);

        final Option output_dir = OptionBuilder.withArgName("output_folder")
                                               .hasArg()
                                               .withDescription("Output directory where parsed movement will be located")
                                               .create(OUTPUT_DIRECTORY);

        final Options options = new Options();
        options.addOption(help);
        options.addOption(rubik_pattern);
        options.addOption(file_pattern);
        options.addOption(output_dir);
        return options;
    }

    private Map<String, List<String>> parseOptions(final Options options) throws ParseException
    {
        List<String> kubikMovementsToParse = new ArrayList<>();
        final CommandLineParser parser = new GnuParser();
        final CommandLine line = parser.parse(options, this.args);
        final Map<String, List<String>> mapOptions = new HashMap<>();


        if (this.args.length == 0 || line.hasOption("help"))
        {
            showUsage(options);
        }

        if (line.hasOption(RUBIK_PATTERN))
        {
            final String optionValue = line.getOptionValue(RUBIK_PATTERN);
            if (StringUtils.isNotBlank(optionValue))
            {
                kubikMovementsToParse.add(optionValue);
            }
        }

        if (line.hasOption(FILE_PATERN))
        {
            final String optionValue = line.getOptionValue(FILE_PATERN);

            if (StringUtils.isNotBlank(optionValue))
            {
                URL url = getUrlFromFile(optionValue);

                if (url != null)
                {
                    try
                    {
                        BufferedReader in;
                        in = new BufferedReader(new InputStreamReader(url.openStream()));

                        String inputLine;
                        while ((inputLine = in.readLine()) != null)
                        {
                            kubikMovementsToParse.add(inputLine);
                        }
                        in.close();
                    }
                    catch (IOException e)
                    {
                        LOGGER.warn("Could not open the input stream {}: ", e.getMessage());
                    }
                }
            }
        }

        if (line.hasOption(OUTPUT_DIRECTORY))
        {
            String value = line.getOptionValue(OUTPUT_DIRECTORY);

            URL url = getUrlFromFile(value);
            File file = FileUtils.toFile(url);
            try
            {
                if (file == null || !file.exists())
                {

                    file = new File(value);
                    FileUtils.forceMkdir(file);
                }
                else
                {
                    FileUtils.cleanDirectory(file);
                }
            }
            catch (IOException e)
            {
                LOGGER.warn("Could not create/clean directory <{}> ", value);
            }


            try
            {
                List<String> outputDirectories = new ArrayList<>();
                outputDirectories.add(file.getCanonicalPath());
                mapOptions.put(OUTPUT_DIRECTORY, outputDirectories);
            }
            catch (IOException e)
            {
                LOGGER.warn("Could not get directory <{}> <{}>", value, e.getMessage());
            }
        }


        mapOptions.put(RUBIK_PATTERN, kubikMovementsToParse);

        return mapOptions;
    }

    private URL getUrlFromFile(String path)
    {
        URL url = null;
        final String unixTypePath = path.replace("\\", "/");
        try
        {
            url = getUrlByURI(new URI(unixTypePath));
        }
        catch (URISyntaxException | IOException e)
        {
            try
            {
                url = getUrlByFile(unixTypePath);
            }
            catch (IOException e1)
            {
                LOGGER.error("Not possible to load the file: {} ", e1.getMessage());
            }
        }
        return url;
    }

    private URL getUrlByURI(final URI configURI) throws IOException
    {
        URL url = configURI.toURL();
        url.openStream().close(); // catches well-formed but bogus URLs
        return url;
    }

    private URL getUrlByFile(final String configUrl) throws IOException
    {
        URL url = null;
        final URI configURI = new File(configUrl).toURI();
        try
        {
            url = getUrlByURI(configURI);
        }
        catch (Exception e)
        {
            LOGGER.warn("Caught exception: {}. A new try to load it by file will be resource will be called", e.getMessage());
            throw new IOException("Could not get URL: " + e.getMessage());
        }
        return url;
    }

    private static void showUsage(Options options)
    {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("kubik_parser", options, true);
    }

    @Override
    public boolean equals(final Object o)
    {
        return EqualsBuilder.reflectionEquals(this, o);
    }

    @Override
    public int hashCode()
    {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public String toString()
    {
        return ToStringBuilder.reflectionToString(this, SHORT_PREFIX_STYLE);
    }
}
